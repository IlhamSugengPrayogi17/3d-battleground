﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClientSend : MonoBehaviour
{
    private static void SendTCPData(Packet _packet)
    {
        _packet.WriteLength();
        Clients.instance.tcp.SendData(_packet);
    }

    private static void SendUDPData(Packet _packet)
    {
        _packet.WriteLength();
        Clients.instance.udp.SendData(_packet);
    }

    #region Packets
    public static void WelcomeReceived()
    {
        using (Packet _packet = new Packet((int)ClientPackets.welcomeReceived))
        {
            _packet.Write(Clients.instance.myId);

            SendTCPData(_packet);
        }
    }

    public static void PlayerMovement(bool[] _inputs)
    {
        using (Packet _packet = new Packet((int)ClientPackets.PlayerMovement))
        {
            _packet.Write(_inputs.Length);
            foreach (bool _input in _inputs)
            {
                _packet.Write(_input);
            }
            _packet.Write(GameManager.instance.players[Clients.instance.myId].transform.rotation);

            SendUDPData(_packet);
        }
    }

    public static void LoginInput(string _username, string _password)
    {
        using (Packet _packet = new Packet((int)ClientPackets.loginInput))
        {
            _packet.Write(_username);
            _packet.Write(_password);

            SendTCPData(_packet);
        }
    }
    
    public static void SignUpInput(string _username, string _password)
    {
        using (Packet _packet = new Packet((int)ClientPackets.signUpInput))
        {
            _packet.Write(_username);
            _packet.Write(_password);

            SendTCPData(_packet);
        }
    }

    public static void isReadyInput(bool _isReady)
    {
        using (Packet _packet = new Packet((int)ClientPackets.isReady))
        {
            _packet.Write(_isReady);

            SendTCPData(_packet);
        }
    }

    #endregion
}
