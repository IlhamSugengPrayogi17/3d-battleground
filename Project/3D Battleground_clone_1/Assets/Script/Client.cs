﻿using UnityEngine;
using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Collections;
using System.Collections.Generic;
using TMPro;

public class Client : MonoBehaviour
{
    public TMP_Text textBox;
    public TMP_InputField textFromClient;
    StreamReader reader;
    StreamWriter writer;
    TcpClient client = new TcpClient("127.0.0.1", 7412);

    private void Start()
    {
        try
        {
            reader = new StreamReader(client.GetStream());
            writer = new StreamWriter(client.GetStream());
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
        }
    }

    public void SendtoServer()
    {
        writer.WriteLine(textFromClient.text);
        writer.Flush();

        String server_string = reader.ReadLine();
        textBox.text += "\n" + server_string;
    }

    private void OnDisable()
    {
        reader.Close();
        writer.Close();
        client.Close();
    }
}
