
using System;
using TMPro;
using UnityEngine;

public class HealthScript : MonoBehaviour
{
    [SerializeField] private TMP_Text healthText;
    public int healthAmount;
    public static HealthScript instance;


    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        healthAmount = 100;
        healthText = GetComponent<TMP_Text>();
    }

    private void Update()
    {
        healthText.text = healthAmount.ToString();
        if (healthAmount <= 0)
        {
            healthAmount = 0;
        }
    }
}
