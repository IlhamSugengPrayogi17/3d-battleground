﻿using System;
using UnityEngine;

public class Coin : MonoBehaviour
{
    public int coinId;

    public void Initialize(int _coinId)
    {
        coinId = _coinId;
    }

    private void Update()
    {
        transform.Rotate (0,0,50*Time.deltaTime);
    }
}