﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    //public bool backLobby;

    public Dictionary<int, PlayerManager> players = new Dictionary<int, PlayerManager>();
    public static Dictionary<int, Coin> coins = new Dictionary<int, Coin>();
    public Dictionary<int, ProjectileManager> projectiles = new Dictionary<int, ProjectileManager>();

    public Coin coinPrefab;

    [Header("Player Prefabs")]
    public GameObject localPlayerPrefab;
    public GameObject playerPrefab;
    public GameObject projectilePrefab;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Debug.Log("Instance already exists, destroying object!");
            Destroy(this);
        }
    }

    public void Start()
    {
        Clients.instance.ConnectToServer();
    }

    public void SpawnPlayer(int _id, string _username, Vector3 _position, Quaternion _rotation)
    {
        GameObject _player;
        if (_id == Clients.instance.myId)
        {
            _player = Instantiate(localPlayerPrefab, _position, _rotation);
        }
        else
        {
            _player = Instantiate(playerPrefab, _position, _rotation);
        }

        _player.transform.SetParent(this.transform, false);
        _player.GetComponent<PlayerManager>().Initialize(_id, _username);
        players.Add(_id, _player.GetComponent<PlayerManager>());
    }

    public void SpawnCoin(int _coinId, Vector3 _position)
    {
        Coin coin = Instantiate(coinPrefab, _position, coinPrefab.transform.rotation) as Coin;
        coin.Initialize(_coinId);
        coins.Add(_coinId, coin);
    }

    public void DestroyCoin(int _coinId)
    {
        Destroy(coins[_coinId].gameObject);
        coins.Remove(_coinId);
    }

    public void SpawnProjectile(int _id, Vector3 _position)
    {
        GameObject _projectile = Instantiate(projectilePrefab, _position, Quaternion.identity);
        _projectile.GetComponent<ProjectileManager>().Initialize(_id);
        projectiles.Add(_id, _projectile.GetComponent<ProjectileManager>());
    }
}