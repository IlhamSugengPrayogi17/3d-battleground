﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFoll : MonoBehaviour
{
    public float camHeight = 2.2f;
    public float camDist = 5.8f;
    //private float distance = 1f;

    [Range(0.01f, 1.0f)]
    public float SmoothFactor = 1f;
    public bool LookAtPlayer = true;

    private Transform localPlayer;
    private Vector3 cameraOffset;

    private void Start()
    {
        StartCoroutine(GetLocalPlayer());
    }

    private void LateUpdate()
    {
        if (localPlayer == null || !LookAtPlayer) return;

        Vector3 newPos = localPlayer.position + cameraOffset;
        transform.position = Vector3.Slerp(transform.position, newPos, SmoothFactor);

        transform.LookAt(localPlayer);
    }

    private IEnumerator GetLocalPlayer()
    {
        yield return new WaitUntil(() => GameManager.instance.players.ContainsKey(Clients.instance.myId));

        localPlayer = GameManager.instance.players[Clients.instance.myId].transform;
        transform.position = new Vector3(localPlayer.position.x, localPlayer.position.y + camHeight, localPlayer.position.z - camDist);
        cameraOffset = transform.position - localPlayer.position;
    }

}
