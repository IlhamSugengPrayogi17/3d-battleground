﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomSpawner : MonoBehaviour
{
    public GameObject itemPrefab;
    public float Rad = 0f;
    public float nextSpawn;

    private float[] linePos = { 3.03f, 1.25f, -0.55f, -2.15f, -2.35f };
    float randomX;
    int randomY;
    Vector2 SpawnSpot;

    private void Update()
    {
        if (Time.time > nextSpawn)
        {
            randomX = Random.Range(-3f, 5f);
            randomY = Random.Range(0, linePos.Length);
            SpawnSpot = new Vector2(randomX, linePos[randomY]);
            Instantiate(itemPrefab, SpawnSpot, Quaternion.identity);

            nextSpawn = Time.time + Rad;
            Debug.Log(Time.time + Rad);
        }
    }
}
