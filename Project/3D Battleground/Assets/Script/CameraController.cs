﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public PlayerManager player;
    public float sendivity = 100f;
    public float angle = 80f;

    private float verticalRot;
    private float horizontalRot;

    private void Start()
    {
        verticalRot = transform.localEulerAngles.x;
        horizontalRot = transform.localEulerAngles.y;
    }

    private void Update()
    {
        LookAt();
        Debug.DrawRay(transform.position, transform.forward * 2, Color.red);
    }

    private void LookAt()
    {
        float _verticalMove = -Input.GetAxis("Mouse Y");
        float _horizontalMove = Input.GetAxis("Mouse X");

        verticalRot += _verticalMove * sendivity * Time.deltaTime;
        horizontalRot += _horizontalMove * sendivity * Time.deltaTime;

        verticalRot = Mathf.Clamp(verticalRot, -angle, angle);

        transform.localRotation = Quaternion.Euler(verticalRot, 0f, 0f);
        player.transform.rotation = Quaternion.Euler(0f, horizontalRot, 0f);
    }
}
