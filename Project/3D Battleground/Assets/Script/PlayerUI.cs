﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PlayerUI : MonoBehaviour
{
    public static PlayerUI instance;

    public GameObject usernamePanel;
    public TextMeshProUGUI usernamePlayerText;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Debug.Log("Instance already exists, destroying object!");
            Destroy(this);
        }
    }

    public void SetUsername(string _usernamePlayer)
    {
        usernamePlayerText.text = " " + _usernamePlayer;
    }
}
