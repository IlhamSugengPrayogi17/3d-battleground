﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GameUIManager : MonoBehaviour
{
    public static GameUIManager instance;

    public GameObject PlayerTotalCanvas;
    public GameObject PlayerReadyCanvas;

    public GameObject winCanvas;
    public GameObject loseCanvas;

    public TextMeshProUGUI totalPlayer;
    public TextMeshProUGUI readyPlayer;

    public TextMeshProUGUI coinText;
    public TextMeshProUGUI healthText;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Debug.Log("Instance already exists, destroying object!");
            Destroy(this);
        }
    }

    public void SetTotalPlayer(int _totalPlayer)
    {
        totalPlayer.text = "Total Player : " + _totalPlayer;
    }

    public void SetReadyPlayer(int _readyPlayer)
    {
        readyPlayer.text = "Player Ready : " + _readyPlayer;
    }

    public void SetCoin(int _coinAmount)
    {
        coinText.text = "Coin : " + _coinAmount;
    }

    public void SetHealth(float _healthAmount)
    {
        healthText.text = "Health : " + _healthAmount;
    }

    public void ShowWinner(int _winnerId)
    {
        if (Clients.instance.myId == _winnerId)
        {
            winCanvas.SetActive(true);
        }
        else
        {
            loseCanvas.SetActive(true);
        }
    }
    
}
