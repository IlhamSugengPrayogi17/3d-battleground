﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BackLobby : MonoBehaviour
{
    public static BackLobby instance;
    public GameObject WinPanel;
    public GameObject LosePanel;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this);
        }
    }

    public void BacktoLobby()
    {
        bool isBack = GameManager.instance.players[Clients.instance.myId].isBack;

        ClientSend.isBackInput(!isBack);
        Debug.Log("Kirim ke Lobby");
    }
}
