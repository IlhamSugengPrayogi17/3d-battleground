﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ScoreScript : MonoBehaviour
{
    [SerializeField] private TMP_Text text;
    public int coinsAmount;
    public static ScoreScript inst;
    public GameObject WinText;

    private void Awake()
    {
        inst = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        coinsAmount = 0;
        text = GetComponent<TMP_Text>();
        WinText.SetActive(false);
    }

    public void AddCoins(int value)
    {
        coinsAmount += value;
    }

    // Update is called once per frame
    void Update()
    {
        text.text = coinsAmount.ToString();
        if (coinsAmount == 5)
        {
            WinText.SetActive(true);
            Time.timeScale = 0;
        }
    }
}
