﻿/*
 *   Copyright (c) 2021 NotSlimBoy
 *   All rights reserved.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace server
{
    class Server
    {
        private static TcpListener listener;
        private static Dictionary<int, User> UserList = new Dictionary<int, User>();
        private static Dictionary<int, TcpClient> ClientList = new Dictionary<int, TcpClient>();
        private static Dictionary<int, Health> HealthList = new Dictionary<int, Health>();
        private static IFormatter formatter = new BinaryFormatter();
        private static int clientCounter;
        public static int CoinCounter;
        public static Coin coin = new Coin(0);
        private static string[] Username = { "Ilham", "Raka", "Prakoso" };
        private static string[] Password = { "Ilham", "Raka", "Prakoso" };

        private static void ProcessClientRequests(object argument)
        {
            int ID = clientCounter;
            Health health = new Health(100);
            Coin coin = new Coin(0);
            TcpClient client = (TcpClient)argument;
            NetworkStream networkStream = client.GetStream();
            StreamReader reader = new StreamReader(client.GetStream());

            ClientList.Add(ID, client);

            ConnectionHandler(client, ID);
            HealthList.Add(ID, health);
            try
            {
                while (client != null)
                {
                    coin.value = 0;
                    string fromclient = reader.ReadLine();
                    Console.WriteLine(fromclient);
                    
                    if (fromclient.Contains("coins"))
                    {
                        UpdateCoin(client);
                    }
                    else if (fromclient.Contains("damage"))
                    {
                        TakeDamage(client, ID);
                    }                    
                }

                reader.Close();
                client.Close();
                Console.WriteLine("Closing client connection!");
            }
            catch (Exception E)
            {
                Console.WriteLine("Problem with client communication. Exiting thread." + E);
            }
            finally
            {
                if (client != null)
                {
                    client.Close();
                }
            }
        }

        public static void SendActionChat(string content)
        {
            Console.WriteLine(ClientList.Values.Count);
            foreach (var client in ClientList.Values)
            {
                TcpBroadCast("actionChat : " + content,client);
            }
            
        }

        public static bool CheckUserLogin(User user, int ID)
        {
            if (user != null)
            {
                for (int i = 0; i < Username.Length; i++)
                {
                    if (user.username == Username[i] && user.password == Password[i])
                    { 
                        UserList.Add(ID, user);
                        return true;
                    }
                }
                return false;
            }
            else
            {
                Console.WriteLine("Account doesn't exist");
                return false;
            }
        }

        public static void TcpBroadCast(string _data, TcpClient client)
        {
            StreamWriter _writer = new StreamWriter(client.GetStream());
            _writer.WriteLine(_data);
            _writer.Flush();
        }

        public static void TakeDamage(TcpClient client, int ID)
        {
            Random random = new Random();
            int damage = random.Next(0, 100);
            NetworkStream networkStream = client.GetStream();
            formatter.Binder = new CustomizedBinder();
            Health health = (Health) formatter.Deserialize(networkStream);
            health.value -= damage;
            Console.WriteLine(client + " Health decreased by " + health.value + " Point's");

            foreach (KeyValuePair<int, Health> _client in HealthList)
            {
                if (ID != _client.Key)
                {
                    HealthList[_client.Key].value -= damage;
                    if (health.value <= 0)
                    {
                        health.value = 0;
                    }
                }
            } 

            foreach (KeyValuePair<int, TcpClient> _client in ClientList)
            {
                if (_client.Value != client)
                {                    
                    StreamWriter writer = new StreamWriter(_client.Value.GetStream());
                    writer.WriteLine("damage");
                    writer.Flush();
                    networkStream = _client.Value.GetStream();
                    formatter.Serialize(networkStream, HealthList[_client.Key]);
                }
            }
            SendActionChat("Your Health Decreased : " + client.ToString());
        }

        static void ConnectionHandler(TcpClient client, int ID)
        {
            NetworkStream networkStream = client.GetStream();

            formatter.Binder = new CustomizedBinder();
            User user = (User)formatter.Deserialize(networkStream);

            if (CheckUserLogin(user, ID))
            {
                Console.WriteLine("Another User Login : " +  user.username);
                TcpBroadCast("Another User Login : " + user.username, client);
            }
            
            else
            {
                Console.WriteLine("Connection Error, Account Doesn't Exist");
            }
        }

        public static void UpdateCoin(TcpClient client)
        {
            NetworkStream networkStream = client.GetStream();
            formatter.Binder = new CustomizedBinder();
            Coin coin = (Coin)formatter.Deserialize(networkStream);
            coin.value++;
            Console.WriteLine("Client wants : " + coin.value);
            //writer.WriteLine("coins");
            //writer.Flush();
            foreach (KeyValuePair<int, TcpClient> _client in ClientList)
            {
                
                if (_client.Value == client)
                {
                    StreamWriter writer = new StreamWriter(_client.Value.GetStream());
                    writer.WriteLine("coins");
                    writer.Flush();
                    networkStream = _client.Value.GetStream();
                    formatter.Serialize(networkStream, coin);
                }
                
            }
            SendActionChat("Your Coin Updated : " + client.ToString());
        }
        
        public static void Main()
        {
            TcpListener listener = null;
            clientCounter = 0;
            try
            {
                listener = new TcpListener(IPAddress.Parse("127.0.0.1"), 7412);
                listener.Start();
                Console.WriteLine("MultiThreadedEchoServer started...");
                while (true)
                {
                    Console.WriteLine("Waiting for incoming client connections...");
                    TcpClient client = listener.AcceptTcpClient();
                    Console.WriteLine("Accepted new client connection...");
                    clientCounter++;
                    Thread t = new Thread(ProcessClientRequests);
                    t.Start(client);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
    }

    sealed class CustomizedBinder : SerializationBinder
    {
        public override Type BindToType(string assemblyName, string typeName)
        {
            Type returntype = null;
            string sharedAssemblyName = "SharedAssembly, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null";
            assemblyName = Assembly.GetExecutingAssembly().FullName;
            typeName = typeName.Replace(sharedAssemblyName, assemblyName);
            returntype = Type.GetType(String.Format("{0}, {1}", typeName, assemblyName));

            return returntype;
        }

        public override void BindToName(Type serializedType, out string assemblyName, out string typeName)
        {
            base.BindToName(serializedType, out assemblyName, out typeName);
            assemblyName = "SharedAssembly, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null";
        }
    }


    public static class ByteArraySerializer
    {
        // from obj to byte
        public static byte[] Serializer<T>(this T serializer)
        {
            using (var ms = new MemoryStream())
            {
                new BinaryFormatter().Serialize(ms, serializer);
                return ms.ToArray();
            }
        }

        // from byte to object
        public static T Deserializer<T>(this byte[] deserializer)
        {
            using (var ms = new MemoryStream(deserializer))
            {
                return (T)new BinaryFormatter().Deserialize(ms);
            }
        }
    }

}