﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using System.Net;
using System.Net.Sockets;

namespace PacketServer
{
    class ServerSend
    {
        private static void SendTCPData(int _toClient, Packet _packet)
        {
            _packet.WriteLength();
            Server.clients[_toClient].tcp.SendData(_packet);
        }

        private static void SendUDPData(int _toClient, Packet _packet)
        {
            _packet.WriteLength();
            Server.clients[_toClient].udp.SendData(_packet);
        }

        private static void SendTCPDataToAll(Packet _packet)
        {
            _packet.WriteLength();
            for (int i = 1; i <= Server.MaxPlayers; i++)
            {
                Server.clients[i].tcp.SendData(_packet);
            }
        }
        private static void SendTCPDataToAll(int _exceptClient, Packet _packet)
        {
            _packet.WriteLength();
            for (int i = 1; i <= Server.MaxPlayers; i++)
            {
                if (i != _exceptClient)
                {
                    Server.clients[i].tcp.SendData(_packet);
                }
            }
        }

        private static void SendTCPDataToAllPlayers(Packet _packet)
        {
            _packet.WriteLength();
            for (int i = 1; i <= Server.MaxPlayers; i++)
            {
                if (Server.clients[i].player != null)
                {
                    Server.clients[i].tcp.SendData(_packet);
                }
            }
        }

        private static void SendUDPDataToAll(Packet _packet)
        {
            _packet.WriteLength();
            for (int i = 1; i <= Server.MaxPlayers; i++)
            {
                Server.clients[i].udp.SendData(_packet);
            }
        }
        private static void SendUDPDataToAll(int _exceptClient, Packet _packet)
        {
            _packet.WriteLength();
            for (int i = 1; i <= Server.MaxPlayers; i++)
            {
                if (i != _exceptClient)
                {
                    Server.clients[i].udp.SendData(_packet);
                }
            }
        }

        private static void SendUDPDataToAllPlayers(Packet _packet)
        {
            _packet.WriteLength();
            for (int i = 1; i <= Server.MaxPlayers; i++)
            {
                if (Server.clients[i].player != null)
                {
                    Server.clients[i].udp.SendData(_packet);
                }
            }
        }

        #region Packets
        public static void Welcome(int _toClient, string _msg)
        {
            using (Packet _packet = new Packet((int)ServerPackets.welcome))
            {
                _packet.Write(_msg);
                _packet.Write(_toClient);

                SendTCPData(_toClient, _packet);
            }
        }

        public static void Login(int _clientID, bool _canLogin, string _message, User _user)
        {
            using (Packet _packet = new Packet((int)ServerPackets.login))
            {
                _packet.Write(_canLogin);
                _packet.Write(_message);

                if (_canLogin)
                {
                    _packet.Write<User>(_user);
                }

                SendTCPData(_clientID, _packet);
            }
        }

        public static void SpawnPlayer(int _toClient, Player _player)
        {
            using (Packet _packet = new Packet((int)ServerPackets.SpawnPlayer))
            {
                _packet.Write(_player.id);
                _packet.Write(_player.username);
                _packet.Write(_player.transform.position);
                _packet.Write(_player.transform.rotation);

                SendTCPData(_toClient, _packet);
            }
        }

        public static void SpawnCoin(int _coinId, Vector3 _position)
        {
            using (Packet _packet = new Packet((int)ServerPackets.spawnCoin))
            {
                _packet.Write(_coinId);
                _packet.Write(_position);
                SendTCPDataToAllPlayers(_packet);
            }
        }

        public static void DestroyCoin(int _coinId)
        {
            using (Packet _packet = new Packet((int)ServerPackets.destroyCoin))
            {
                _packet.Write(_coinId);
                SendTCPDataToAllPlayers(_packet);
            }
        }

        public static void PlayerCoin(int _toClient, int _coinAmount)
        {
            using (Packet _packet = new Packet((int)ServerPackets.playerCoin))
            {
                _packet.Write(_coinAmount);
                SendTCPData(_toClient, _packet);
            }
        }

        public static void PlayerPosition(Player _player)
        {
            using (Packet _packet = new Packet((int)ServerPackets.PlayerPosition))
            {
                _packet.Write(_player.id);
                _packet.Write(_player.transform.position);

                SendUDPDataToAllPlayers(_packet);
            }
        }

        public static void PlayerRotation(Player _player)
        {
            using (Packet _packet = new Packet((int)ServerPackets.PlayerRotation))
            {
                _packet.Write(_player.id);
                _packet.Write(_player.transform.rotation);

                SendUDPDataToAllPlayers(_packet);
            }
        }

        public static void SendPlayerAnimation(int _id, string animation, bool _value)
        {
            using (Packet _packet = new Packet((int)ServerPackets.PlayerAnimation))
            {
                _packet.Write(_id);
                _packet.Write(animation);
                _packet.Write(_value);

                SendUDPDataToAllPlayers(_packet);
            }
        }


        public static void SignUp(int _clientID, bool _canLogin, string _message)
        {
            using (Packet _packet = new Packet((int)ServerPackets.signUp))
            {
                _packet.Write(_canLogin);
                _packet.Write(_message);

                SendTCPData(_clientID, _packet);
            }
        }

        public static void PlayerDisconnected(int _clientID)
        {
            using (Packet _packet = new Packet((int)ServerPackets.PlayerDisconnected))
            {
                _packet.Write(_clientID);

                SendTCPDataToAll(_packet);
            }
        }

        public static void PlayerIsReady(int _clientId, bool _isReady)
        {
            using (Packet _packet = new Packet((int)ServerPackets.playerIsReady))
            {
                _packet.Write(_clientId);
                _packet.Write(_isReady);

                SendTCPDataToAllPlayers(_packet);
            }
        }

        public static void TotalIsReadyPlayer(int _totalPlayer, int _totalIsReadyPlayer)
        {
            using (Packet _packet = new Packet((int)ServerPackets.totalIsReadyPlayer))
            { 
                _packet.Write(_totalPlayer);
                _packet.Write(_totalIsReadyPlayer);

                SendTCPDataToAllPlayers(_packet);
            }
        }

        public static void PlayerIsBack(int _clientId, bool _isBack)
        {
            using (Packet _packet = new Packet((int)ServerPackets.playerIsReady))
            {
                _packet.Write(_clientId);
                _packet.Write(_isBack);

                SendTCPDataToAllPlayers(_packet);
            }
        }

        public static void StartGame()
        {
            using (Packet _packet = new Packet((int)ServerPackets.startGame))
            {
                SendTCPDataToAllPlayers(_packet);
            }
        }

        public static void BackLobby()
        {
            using (Packet _packet = new Packet((int)ServerPackets.backLobby))
            {
                SendTCPDataToAllPlayers(_packet);
            }
        }

        public static void PlayerHealth(Player _player)
        {
            using (Packet _packet = new Packet((int)ServerPackets.playerHealth))
            {
                _packet.Write(_player.id);
                _packet.Write(_player.health);

                SendTCPDataToAll(_packet);
            }
        }

        public static void PlayerRespawned(Player _player)
        {
            using (Packet _packet = new Packet((int)ServerPackets.playerRespawned))
            {
                _packet.Write(_player.id);

                SendTCPDataToAll(_packet);
            }
        }

        public static void SpawnProjectile(Projectil _projectile, int _thrownByPlayer)
        {
            using (Packet _packet = new Packet((int)ServerPackets.spawnProjectile))
            {
                _packet.Write(_projectile.id);
                _packet.Write(_projectile.transform.position);
                _packet.Write(_thrownByPlayer);

                SendTCPDataToAll(_packet);
            }
        }

        public static void ProjectilePosition(Projectil _projectile)
        {
            using (Packet _packet = new Packet((int)ServerPackets.projectilePosition))
            {
                _packet.Write(_projectile.id);
                _packet.Write(_projectile.transform.position);

                SendTCPDataToAll(_packet);
            }
        }

        public static void ProjectileExploded(Projectil _projectile)
        {
            using (Packet _packet = new Packet((int)ServerPackets.projectileExploded))
            {
                _packet.Write(_projectile.id);
                _packet.Write(_projectile.transform.position);

                SendTCPDataToAll(_packet);
            }
        }

        public static void WinnerPlayer(int _clientId)
        {
            using (Packet _packet = new Packet((int)ServerPackets.winnerPlayer))
            {
                _packet.Write(_clientId);

                SendTCPDataToAll(_packet);
            }
        }

        #endregion
    }
}
