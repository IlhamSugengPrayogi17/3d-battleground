﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace PacketServer
{
    public class BackLobby : MonoBehaviour
    {
        public static BackLobby instance;

        public int minPlayer = 2;

        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }
            else
            {
                Destroy(this);
            }
        }

        public void CheckBackLobby()
        {
            int totalPlayer = GetTotalPlayer();
            int isReadyPlayer = GetIsReadyPlayer();

            if (totalPlayer >= minPlayer && totalPlayer == isReadyPlayer)
            {
                ServerSend.BackLobby();
                GameManager.instance.BackLobby();
            }
        }

        public int GetTotalPlayer()
        {
            int countPlayer = 0;
            foreach (Client _client in Server.clients.Values)
            {
                if (_client.player != null)
                {
                    countPlayer++;
                }
            }
            return countPlayer;
        }

        public int GetIsReadyPlayer()
        {
            int countIsReadyPlayer = 0;
            foreach (Client _client in Server.clients.Values)
            {
                if (_client.player != null && _client.player.isReady)
                {
                    countIsReadyPlayer++;
                }
            }
            return countIsReadyPlayer;
        }
    }
}

