﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PacketServer
{
    public class Coin : MonoBehaviour
    {
        public int coinId;
        private float destroyTime = 40f;

        // Start is called before the first frame update
        public void Initialize(int _coinId)
        {
            coinId = _coinId;
            StartCoroutine(DestroyAfter());
        }

        private void Update()
        {
            transform.Rotate (0,0,50*Time.deltaTime);
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.tag == "Player")
            {
                other.transform.parent.gameObject.GetComponent<Player>().AddCoins();
                DestroyCoin();
            }
        }

        private void DestroyCoin()
        {
            ServerSend.DestroyCoin(coinId);
            CoinSpawn.coins.Remove(coinId);
            Destroy(gameObject);
        }

        IEnumerator DestroyAfter()
        {
            yield return new WaitForSeconds(destroyTime);
            DestroyCoin();
        }
    }
}

