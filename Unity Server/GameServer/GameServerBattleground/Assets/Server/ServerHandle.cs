﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace PacketServer
{
    class ServerHandle
    {
        public static void WelcomeReceived(int _fromClient, Packet _packet)
        {
            int _clientIdCheck = _packet.ReadInt(); 

            Debug.Log($"{Server.clients[_fromClient].tcp.socket.Client.RemoteEndPoint} connected successfully and is now player {_fromClient}.");
            if (_fromClient != _clientIdCheck)
            {
                Debug.Log($"Player (ID: {_fromClient}) has assumed the wrong client ID ({_clientIdCheck})!");
            }
        }

        public static void LoginInput(int _fromClient, Packet _packet)
        {
            string _username = _packet.ReadString();
            string _password = _packet.ReadString();
            AuthManager.instance.LoginInput(_fromClient,_username,_password);
        }
        
        public static void SignUpInput(int _fromClient, Packet _packet)
        {
            string _username = _packet.ReadString();
            string _password = _packet.ReadString();
            AuthManager.instance.SignUpInput(_fromClient,_username,_password);
        }

        public static void IsReadyInput(int _fromClient, Packet _packet)
        {
            bool _isReady = _packet.ReadBool();
            //int _totalReadyPlayer = _packet.ReadInt();
            Debug.Log("Terima");

            Server.clients[_fromClient].player.isReady = _isReady;
            ServerSend.PlayerIsReady(_fromClient, _isReady);

            LobbyManager.instance.CheckStartGame();
        }

        public static void IsBackInput(int _fromClient, Packet _packet)
        {
            bool _isBack = _packet.ReadBool();
            Debug.Log("Terima");

            Server.clients[_fromClient].player.isBack = _isBack;
            ServerSend.PlayerIsBack(_fromClient, _isBack);
            Debug.Log("kirim");

            //LobbyManager.instance.CheckStartGame();
            BackLobby.instance.CheckBackLobby();
        }

        public static void PlayerShoot(int _fromClient, Packet _packet)
        {
            Vector3 _shootDirection = _packet.ReadVector3();

            Server.clients[_fromClient].player.Shoot(_shootDirection);
            Server.clients[_fromClient].player.ThrowItem(_shootDirection);
        }
        

        public static void PlayerMovement(int _fromClient, Packet _packet)
        {
            bool[] _inputs = new bool[_packet.ReadInt()];
            for (int i = 0; i < _inputs.Length; i++)
            {
                _inputs[i] = _packet.ReadBool();
            }
            Quaternion _rotation = _packet.ReadQuaternion();

            Server.clients[_fromClient].player.SetInput(_inputs, _rotation);
        }
    }
}
