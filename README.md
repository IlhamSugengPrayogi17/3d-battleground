# Battleground
### _3D Arcade-Third Person Shooter Game_
![Title1](/uploads/ea9ebcfe459ecbf2a6dc8675e254dba4/Title1.png)

### _Gameplay_
![ezgif.com-gif-maker__3_](/uploads/8f047a5b8db2782d2ae0add2fd1c05d9/ezgif.com-gif-maker__3_.gif)

Battleground is a PC Multiplayer Online Arcade Third Person Shooter Game

### Development Team
4210181022 - [Ilham Sugeng Prayogi](https://gitlab.com/IlhamSugengPrayogi17)

4210181030 - [Raka Arya pratama](https://gitlab.com/notslimboy)

### Table Of Content

[[_TOC_]]

### Technologies

    - Unity 2019.4.12f1

### Overview Gameplay
Battleground is an online multiplayer game with the Arcade-Third Person Shooter genre, where players must collect coins up to a certain amount to win, but players will also compete with other players for coins that are randomly spawned on the battlefield. Shooter will be the main mechanic for each player to interfere with the progress of other players to get more coins, by making other players die and making the coins they have collected back to zero

_Game Flow_
![Ada](/uploads/9f5875074b1d671360621a18405bfd2b/Ada.png)

### Document of Detail

High Concept Game       : [Battleground](https://docs.google.com/presentation/d/1qv1w462lPm_R11gS_0Mq-WkMAVuSzctbSRvv-UK2Hx0/edit#slide=id.gc4dcfbf537_0_221)

Game Design Document    : [GDD](https://docs.google.com/document/d/1LVZFuZOb0D2SCash1Bt0_2hJxKAmFcmL5va6IGfP9oM/edit#heading=h.pv9zxyvats0l)

### Documentation

There are several features and functions that are the main foundation of this game, here are the explanations:

#### Packet System

The packet system is the main feature that acts as a bridge between the client and the server to facilitate data exchange, this system has the same function as a multithreaded system which is used to make multiple client systems connected to the same server. The workings of this Packet System is to make the data that you want to send, either from the client or the server will be wrapped in packets, where each packet will have its own identity and destination address. This will make the data can be delivered more accurately and quickly to the destination.

The following are the parts that make up this Packet System so that it can run to become a bridge between multi-clients and servers:

    - Packet Identifier
    - Packet Handler (Handle & Send)
    - Client / Server Function
    - Protocol / Data Transfer

_Packet Flow_
![Flow](/uploads/481bb0b54ef32bfa0b6ce46e1b106cae/Flow.png)

    1. Packet Identifier

Packet Identifier has a function to store various kinds of identities that will be used in the process of sending data between the client and server, both sides must have the same program to be identify each data received and also provide identity to the packet want to be sent 

    2. Packet Handler

Packet Handler is used as the part in charge of receiving every packet that is sent, this Packet Handler will be on the server and client, when it has received a packet with its respective identity, the packet handler will immediately provide access to certain functions to process data from the packet that has been received. 

    3. Client / Server Function

The Client / Server function is the main tool used to read input from the player, send data to the Handler (Send), and receive packets from the Handler (Receive) and execute the received data.

    4. Protocol / Data Transfer

This section contains several functions that are used to send data using the TCP or UDP protocol, where both of these protocols are useful for sending data as needed, where the various functions provided can use protocols to send data directly to all clients or only send data / packets to certain clients.


#### Authentication

To play this game, players must go through Authentication which is used to check the identity of the player, whether it is registered as a player in this game or not, here is an explanation of the Authentication System in this game which consists of Log In and Sign Up :

_Authentication Flow_
![LogSignFlow](/uploads/901e4d80325802add1a4725eab8d24a1/LogSignFlow.png)

    1. Log In

a. The first Log In process is that the player must enter data into the provided panel, where this data will be carried by the Login() function in AuthManager, the entered data will be processed into the LoginInput() function in ClientSend

b. The LoginInput() function will provide an identity for the data sent in the form of a packet, the data sent is a username and password

c. The data sent to the server is received by the LoginInput() function in the ServerHandle and the received data is passed to the LoginInput() function in the AuthManager

d. In the loginInput() function in AuthManager this will be used to check whether the data inputted by the player and sent from the client is in accordance with the user data or not. The appropriate data will also activate the SendIntoGame() function where this function will be useful for spawning the player object.

e. Resend data from the server is sent via the Login() function in ServerSend, where the data sent is the id, message, and data bool with the name _canLogin

f. The Login() function in the ClientHandle is in charge of receiving the packet sent and forwarding it to the AuthManager and LobbyManager if the data sent has been successful and the login is successful.

_Login gif_

![Login](/uploads/0c3707e016f019591acf703195beee86/Login.gif)

    2. Sign In

a. If the player does not have an account to log in, then the player can sign up, this process starts from the SignUp() function in AuthManager, where the data entered will be sent via the SignUpInput() function in ClientSend

b. The SignUpInput() function will send data containing the username and password which will be the data for the new user to the server

c. Next, the SignUpInput() function in the ServerHandle will receive the packet and then pass it to the SignUpInput() function in the AuthManager

d. The SignUpInput() function in AuthManager is to check whether the data used to create a new user is appropriate or not, and also this function will activate another function, namely the CreateNewUser() function in the same script.

e. The CreateNewUser() function will create a new user with the data that has been obtained and then add it to the user list

f. When the data for creating a new user is correct and the new user is successfully created, then this information will be given again to the Client via SignUp() on ServerSend, this SignUp() function will send a packet containing id, username, and bool _canLogin to the client

g. The packet will be received by the SignUp() function in the ClientHandle and then forwarded to the AuthManager to give a message if the Sign Up has been successful.

_Sign Up_

![SignUp](/uploads/d95ab7fe00f67f5bc35c9b2e12d9a9c4/SignUp.gif)

#### Lobby System

Before entering the game, there is a Lobby System that players will enter before the game takes place, this Lobby System is used to regulate players as well as the process of moving the scene in this game. The lobby system will be the place where players determine whether they are ready to enter the battlefield or not, the game can only start when the lobby system has received data if all players are ready to play, where the lobby will immediately move players who are already in the lobby to the battlefield . Here's an explanation of how the Lobby System works in this game : 

_Lobby Flow_

![TotalIsReady](/uploads/3b6bc9878bc13377b62bee4b9e7ceec5/TotalIsReady.png)

    1. Get Total Player

a. The lobby will provide information about how many users have entered the lobby and want to join the game, the GetTotalPlayer() function in the LobbyManager on the server is tasked with increasing the total number of users by re-checking the dictionary client on the server..

b. GetTotalPlayer() is related to the CheckStartGame() function where in this function the Server will send data about the number of clients that have entered the lobby, and send the data through the TotalIsReadyPlayer() function on ServerSend.

c. In the ClientHandle, the TotalIsReadyPlayer() function will receive the packet sent and will pass it to the GameUIManager to show the number of players that entered the client UI.

_Total Player_

![TotalPlayer](/uploads/d9d83e386dd299a2c1a0cdd5b23e6756/TotalPlayer.gif)

    2. Get is Ready Player

a. To make the game start, all players must be ready to start the game, this starts from the player entering the lobby and activating the lobby panel, where in this panel there will be a button containing the LobbyManager script, in which there is a ToggleIsReady function ( ) which is used to send data if the Player is ready.

b. The IsReadyInput() function in ClientSend will send a packet containing the _isReady bool data to the Server, to make the isReady condition true.

c. ServerHandle will receive packets via the IsReadyInput() function, which will immediately activate several functions, starting from creating a true isReady boolean in the Player, the PlayerIsReady() function will also be active to send data back to the client, and the CheckStartGame() function to check the state of the specified conditions to start the game.

d. The PlayerIsReady() function will send data in the form of id and bool isReady to the client.

e. In the ClientHandle, the PlayerIsready() function will receive the packet and forward it to the GameManager and then to the Player Manager to provide data if the player with the associated user id is in ready condition.

f. When a player enters the lobby, in addition to the GetTotalPlayer() function, there is another function, namely GetIsreadyPlayer(), where this function is useful for checking the number of players who are already in the ready position.


    3. Start Game and go to battlefield

a. As explained earlier, if the game will only start if all players who have entered the lobby must be in a ready condition, therefore in the LobbyManager script there is a CheckStartGame() function which in this function is when all players in the lobby are in a state ready, then some functions will be active.

b. The first function that is active is StartGame() on ServerSend, this function will send data when the game is ready to be played to the client.

c. On the clientHandle, the StartGame() function will receive the packet and continue its process to the LoadScene() function on the SceneManager, where all players will move the scene to the battlefield or in here is the Residental 1 scene, and the game will start immediately when the player has entered the scene.

d. In addition to the StartGame() function in GameManager, the StartGame() function in GameManager will be active where this function will create spawn coins on the active server

_Player Ready & Game Start_

![ezgif.com-gif-maker](/uploads/5d0514fb4ba4bc649a5096ced878bb4f/ezgif.com-gif-maker.gif)

#### Main Gameplay

There are several main mechanism in this game, here's an explanation of how all these mechanisms work in this online multiplayer game :

##### 1. Spawning Player

The player spawning feature will work when the data entered by the player through the Login panel on the client matches the data in memory on the server, the data will provide confirmation to several functions used for the spawn player process to start active. Players will be spawned in the lobby scene and only transferred to the main game scene when all players are in a "ready" position.

_Spawning Player Flow_

![Spawn](/uploads/5e030a9991b53e7fe3cc1e6152406d0e/Spawn.png)

1. Along with the successful Authentication process, the incoming player data will be sent to the SendIntoGame() function in the client script on the server, in this SendIntoGame() function the data about the player id and username will be used again to perform the player spawn process by activating the SpawnPlayer() on ServerSend.

2. To perform the Spawn Player process, the SendIntoGame() function will take the SpawnPlayer() function in the NetworkManager script which stores data about the Player object, starting from the prefab, position and others.

3. In Server Player will be spawned, along with this process, data about id, username, position and rotation will be sent via packet via PlayerSpawn() function in ServerSend

4. The client will receive the package via the ClientHandle, after getting the package the SpawnPlayer() function in GameManager will be active

5. The SpawnPlayer() function in GameManager will be used to spawn for players on the Client

##### 2. Player Movement

Player movement is the main feature in this game to move player objects, the movement process begins with the client receiving input from the user and then sending the data to the server. On this server, every data is processed to move the player object on the server, and changes in the position or rotation of the object player on the server will be sent back to the client to make the player object also updated.

_Player Movement Flow_

![Movement](/uploads/05d36d91bcec958bab583bdadbc3e5a1/Movement.png)

1. The client gets input from the player, the SendInputToServer() function in the PlayerController becomes the initial stage for the player movement process, where this function will receive input from the player and the input results will relate to the PlayerMovement() function in the ClientSend script.

2. The PlayerMovement() function in ClientSend will send the input results to the server in the form of a packet, which contains the input made by the player, this function will send packets using the UDP protocol


3. The ServerHandle will receive the packet via the PlayerMovement() function, this function will read the contents of the received packet and forward it to the Player script via the SetInput() function.

4. The SetInput() function will get the data inputted by the player, and use it in the player script to update the position depending on the input data received.

5. The input will enter the update function and check directly which input is made by the player, after passing the update, the data will be continued to the Move() function where this function will update the position and rotation of the player according to the input results.

6. These position and rotation updates will be sent via the PlayerPosition() and PlayerRotation() functions on ServerSend, both of which will send the id and update transform to the client.

7. Both functions that have the same name in the ClientHandle will receive the packet and then continue the process to the update transform in GameManager, and make the position and rotation of the player object on the client also updated. 

##### 3. Coin Spawn

The Coin Spawn feature can only be active when the player has entered the main game scene, the server will receive data if all players are in a ready position and will confirm the Spawn coin function to start spawning coins.

_Coin Spawn Flow_

![SpawnCoin](/uploads/c6aa9b2f204edab3bbe9b068ea4f8011/SpawnCoin.png)

1. The CoinSpawn script will be the main script used to manage the Spawn coin process, this script only works when the StartGame() function in the GameManager script is active. When the CoinSpawn script is active, the coin spawn process will run by taking the provided prefabs. In addition to taking prefabs, this script is also used to determine the number of coins that are spawned, the spawn position of each coin, and the spawn deadline between coins. CoinSpawn will also send the id and position of each coin spawned via ServerSend

2. On ServerSend in the SpawnCoin() function the coin data i.e. id and position are sent via packet to the Client

3. The client receives the packet on the ClientHandle and activates the SpawnCoin() function in the GameManager

4. The SpawnCoin() function in GameManager will instantiate the prefab and spawn coin according to the data on the id and position from the Server 

##### 4. Player Shooting

The Player Shooting feature is a meta feature that players can use to fight and interfere with the progress of other players, with this feature players can spawn projectiles and inflict damage on other players. This feature is similar to player movement where input from the player is the main data for using this feature. Every input data received by the client, will be sent to the server and start activating various functions that are used to instantiate the projectile object.

_Player Shooting Flow_

![PlayerShootoo](/uploads/d2b8fef9c3a240c2a090f2929885c508/PlayerShootoo.png)

1. Shooting starts from the client receiving input from the player in the Update() function in the PlayerController Script, which is directly used to activate the PlayerShoot() function in ClientSend.

2. In the PlayerShoot() function in ClientSend the existing facing data is sent to the server in the form of Packets using the TCP protocol media

3. The PlayerShoot() function in the ServerHandle is in charge of receiving packets from the server, and proceeds to the Shoot() and ThrowItem() functions in the Player script.

4. The Shoot() function is used to activate animations and inflict damage on other player objects in the direction of the player's camera

5. As for ThrowItem(), it immediately activates the InstantiateProjectile() function in the NetworkManager script to spawn a projectile

6. The Instantiate Projectile() function which is connected to the projectile script, is used to instantiate data from the projectile starting from prefab, position and quaternion

7. In the projectil script there will be several active functions, starting from the Start() function which will spawn the projectile and send data to the client via ServerSend

8. Next is the FixedUpdate() function which will update the position of the projectile that has been shot and send the data via ServerSend so that the position on the Client will also updated.

9. Then there is the Explode() function which is active when the projectile hits an object with the "Player" tag, it will damage the player object, as well as activate the explosion effect. All this data will also be sent via ServerSend to be updated on the Client.

10. In the ServerSend script, the three functions above will send packets via the TCP protocol

11. All packets of the three functions will be received in the ClientHandle according to their respective data recipients, where everything will be directly connected to the GameManager

12. The ProjectileExploded() function will be connected to the Explode() function in the ProjectileManager script, where the Explode() function will function to instantiate the Explode effect, and destroy the object..

13. In the SpawnProjectile() function in GameManager, there will be an Instantiate on the Projectile object and the Client can finally shoot and spawn the projectile

_Player Shoot_

![PlayerShoot__1_](/uploads/277b35c09aae454c1536726d141cb1c0/PlayerShoot__1_.gif)

##### 5. Player Coin

Coins on the battlefield can be taken by players and increase the number of coins owned by players, these coins will react when there is a collision with the player object.

_Player Coin Flow_

![PlayerCoins](/uploads/3e6c68345d5468cdedf78ecf46fcae45/PlayerCoins.png)

1. Player coin is a system that allows players to get coins after colliding with coin objects in Battlefield, this system starts from the OnTriggerEnter() function in the coin script on the server, where if the coin object collides with an object with the "Player" tag then the function AddCoins() in Player script will be active.

2. This AddCoins() function will add the number of coins owned by the Player to the stats, as well as send the latest data from the number of coins via the PlayerCoin() function on ServerSend.

3. The PlayerCoin() function itself has a function to send data from updating the number of coins owned by the player, so that the Player object in the Client also gets the same update, where the packet sent via the PlayerCoin() function contains the player id and the latest coin number from the player.

4. PlayerCoin() in the ClientHandle will receive the packet sent from the server and directly connect it to the GameManager to update the number of coins on the player with the appropriate id.

5. In addition to going to GameManager, to display the number of coins in the UI, the data that has been obtained by PlayerCoin() will also be passed to the SetCoin() function in GameUIManager.

_Player Coin_

![PlayerCoin__1_](/uploads/b420ed6d63062c9421bf6f61b0fb9eb9/PlayerCoin__1_.gif)

##### 6. Player Health

Player health becomes a meta system that becomes the main stats for players, this health will decrease if the player is hit by projectiles thrown by other players. Health is also the mechanic that forms the basis of the die & respawn feature in this game

_Player Health Flow_

![Playerhealths](/uploads/7e98dad0eec28dbc857005260ab67898/Playerhealths.png)

1. Player Health is a system that is used to regulate the amount of health of the Player after being hit by a projectile, therefore this feature starts from the Explode() function in the projectil script on the server side, where in that function, if the projectile collides with the object tag "Player", then the TakeDamage() function will be active.

2. TakeDamage() itself will set the health on the player after being hit by the projectile, the number of health will be updated and the data will be sent to the Client via the PlayerHealth() function on ServerSend.

3. PlayerHealth() will send health update data to the client by sending a packet containing the latest health amount and client id.

4. The PlayerHealth() function on the ClientHandle will receive a packet from the server, and will continue to update the health data on the player with the appropriate id via GameManager.

5. Update data from health will also be used by the SetHealth() function in GameUIManager to display the latest health data in the Ui Client.

_Player Health_

![ezgif.com-gif-maker__1_](/uploads/0bafa0851a68429322ca69a9e40569d2/ezgif.com-gif-maker__1_.gif)

##### 7. Die & Respawn System

This feature is strongly influenced by the condition of the player's health, when the player's health has reached 0, the die() function will be active and make the player object disappear. At the same time a request for respawn is also given to the server to respawn the player object, with default stats and a position in the specified spawn spot.

_Die & Respawn Flow_
![Respawn](/uploads/a46fe931feb446c505073943f2919fcf/Respawn.png)

1. The Die & Respawn System starts from the TakeDamage() function in the Player script on the server side, in the TakeDamage() function when the health of the player has reached a value less than equal to 0, then the position of the player will be returned to the spawn spot, and position updates This is sent to the client via the PlayerPosition() function on ServerSend, the health update will also be sent via the PlayerHealth() function.

2. Same as sending packets for Player health before, here sending packets via the PlayerHealth() function which contains the id and health updates.

3. In addition to PlayerHealth(), the PlayerPosition() function will also send a packet to the client with the contents of id and transform.position, after sending the position, this function will also coroutine the PlayerRespawn() function.

4. The PlayerRespawn() function on this server respawns the player object with the initial default stats, and the data is also sent to the client via the PlayerRespawned() function on ServerSend.

5. PlayerRespawned() will send data in the form of id from player to client.

6. Back to the health packet, this packet is received by the PlayerHealth() function which will be directly connected to the GameManager and GameUIManager.

7. In GameManager, health which is already in the value 0, will enter the SetHealth function in the PlayerManager script, because the health value is equal to 0 it will activate the Die() function

8. The Die() function has a function to remove the player object because its health is already 0.

9. Returning to Player Respawn, the data from the Respawn packet is received by the PlayerRespawned() function in the ClientHandle, and directly passed to the GameManager connected to the Respawn() function in the PlayerManager script.

10. This Respawn() function is used to reactivate the previously lost player object, return its stats back to default, and because previously the player's position has been reset to the initial position with the PlayerPosition() function which has received new data from the Server, then the respawn position is also in the initial position

_Player Die & Respawn_

![ezgif.com-gif-maker__2_](/uploads/7e09716e35bc165da99ab2b141e4d13c/ezgif.com-gif-maker__2_.gif)

##### 8. Win-Lose System

Win-Lose conditions will be affected by the number of coins owned by the player, when a player has reached the specified number of coins (here, there are 10 coins), the player has won the game, and defeated all the players who participated in the game.

_Win-Lose Flow_

![ShowWinner](/uploads/aaed7029cf042d3750815c60cc291390/ShowWinner.png)

1. Win-Lose system starts from the AddCoins() function on the Server, where in this function if the player's coin is equal to a certain amount (here 10) it will activate the WinnerPlayer() function on ServerSend

2. The WinnerPlayer() function will send the id data from the player on the server to the client who has received 10 coins via TCP protocol to all clients

3. Data from this server will be received in packet form by WinnerPlayer() function in ClientHandle and directly connected to ShowWinner() function in GameUIManager

4. The ShowWinner() function has a function if the received id matches with player id then the Winner panel will be active, while for other players with a different id, the lose panel will be active

_Player Win & Lose_

![Win](/uploads/9166d5b99fb0cfe02579a6264ed9b927/Win.gif)

